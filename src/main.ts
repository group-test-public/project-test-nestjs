import { NestFactory, HttpAdapterHost } from '@nestjs/core';
import { AppModule } from './app.module';
import { MyLoggerService } from './my-logger/my-logger.service';
import { AllExceptionsFilter } from './users/all-exceptions.filter';

async function bootstrap() {
  const app = await NestFactory.create(AppModule
  //   , {
  //   bufferLogs: true}
  );
  
    const {httpAdapter} = app.get(HttpAdapterHost)
    app.useGlobalFilters(new AllExceptionsFilter(httpAdapter))

  // app.useLogger(app.get(MyLoggerService))
  app.enableCors() // giới hạn truy cập
  app.setGlobalPrefix('api')
  await app.listen(3000);
}
bootstrap();
