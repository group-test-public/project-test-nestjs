import { Controller, Query, Get, Param, Post, Body, Patch, Delete, ParseIntPipe, ValidationPipe } from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';

@Controller('users')
export class UsersController {

    constructor(private readonly userService: UsersService){}

    @Get()
    findAll(@Query('role') role?: 'INTERN' | 'ENGINEER' | 'ADMIN')
    {
        return this.userService,this.findAll(role)
    }

    @Get(':id')
    findOne(@Param('id', ParseIntPipe) id:number)
    {
        return this.userService.findOne(id)
    }

    @Post()
    create(@Body(ValidationPipe) CreateUserDto: CreateUserDto)
    {
        return this.userService.create(CreateUserDto)
    }

    @Patch(':id')
    update(@Param('id', ParseIntPipe) id: number, @Body(ValidationPipe) UpdateUserDto: UpdateUserDto)
    {
        return this.userService.update(+id, UpdateUserDto)
    }

    @Delete(':id')
    delete(@Param('id', ParseIntPipe) id: number)
    {
        return this.userService.delete(id)
    }
}
