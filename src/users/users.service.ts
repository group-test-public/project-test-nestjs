import { Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { NotFoundException } from '@nestjs/common';

@Injectable()
export class UsersService {
    private users = [
        {
            "id": 1,
            "name": "ben",
            "email": "hola@gmail.com",
            "role": "INTERN"
        },{
            "id": 2,
            "name": "ben2",
            "email": "hola@gmail.com",
            "role": "ADMIN"
        },{
            "id": 3,
            "name": "ben3",
            "email": "hola@gmail.com",
            "role": "INTERN"
        },{
            "id": 4,
            "name": "ben4",
            "email": "hola@gmail.com",
            "role": "INTERN"
        },{
            "id": 5,
            "name": "ben5",
            "email": "hola@gmail.com",
            "role": "ENGINEER"
        },{
            "id": 6,
            "name": "ben6",
            "email": "hola@gmail.com",
            "role": "INTERN"
        },{
            "id": 7,
            "name": "ben7",
            "email": "hola@gmail.com",
            "role": "ADMIN"
        },{
            "id": 8,
            "name": "ben8",
            "email": "hola@gmail.com",
            "role": "ENGINEER"
        }
    ]

    findAll(role?: 'INTERN' | 'ENGINEER' | 'ADMIN'){
        if(role){
            const rolesArray = this.users.filter(user => user.role === role)
            if(rolesArray.length === 0){
                throw new NotFoundException('User Role Not Found!')
            }
            return rolesArray
        }
        return this.users
    }

    findOne(id: number){
        const user = this.users.find(user => user.id === id)

        if(!user){
            throw new NotFoundException("User not found!")
        }
        return user
    }

    create(CreateUserDto: CreateUserDto)
    {
        const usersByHighestId = [...this.users].sort((a,b) => b.id = a.id)
        const newUser = {
            id: usersByHighestId[0].id + 1,
            ...CreateUserDto
        }

        this.users.push(newUser)
    }

    update(id: number, UpdateUserDto: UpdateUserDto){
        this.users = this.users.map(user => {
            if(user.id === id){
                return {...user, ...UpdateUserDto}
            }
            return user
        })

        return this.findOne(id)
    }

    delete(id: number){
        const removedUser = this.findOne(id)

        this.users = this.users.filter(user => user.id !== id)

        return removedUser
    }
}
