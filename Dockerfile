# Build một container dựa trên image node:20-alpine
# Stage này dùng cho môi trường development
FROM node:20-alpine As development

# Tạo thư mục /usr/src/app trong container
WORKDIR /usr/src/app

# Thêm user ben vào container ( không có password )
# -D is --disabled-password
RUN adduser -D ben 

# Thay đổi quyền sở hữu thư mục /usr/src/app cho user ben và group root
# -R is --recursive ( Thay đổi quyền sở hữu )
RUN chown -R ben:root /usr/src/app

# Copy package.json và package-lock.json vào container,
# Đồng thời thay đổi quyền sở hữu cho user ben và group root
COPY --chown=ben:root package*.json ./

# Cài đặt dependencies của dự án từ package-lock.json 
RUN npm ci

# Copy toàn bộ code của dự án vào container,
# đồng thời thay đổi quyền sở hữu cho user ben và group root
COPY --chown=ben:root . .

# Chuyển sang sử dụng user ben để chạy lệnh tiếp theo
USER ben

# Khai báo cổng 8888 mà ứng dụng Node.js sẽ lắng nghe bên trong container
EXPOSE 8888

# Chạy lệnh "node index.js" khi container được khởi động
CMD ["node", "index.js"]

# >>> stage build <<< #
FROM node:18-alpine As build

WORKDIR /usr/src/app

COPY --chown=ben:root package*.json .

COPY --chown=ben:root --from=development /usr/src/app/node_modules ./node_modules

COPY --chown=ben:root . .

RUN npm run build

# Chỉ tải xuống các dependencies cho image product,
# loại bỏ cache npm không cần thiết
RUN npm ci --only=production && npm cache clean --force

USER ben

# >>> stage production <<< #
FROM node:18-alpine As production

WORKDIR /usr/src/app 

COPY --chown=ben:root --from=build /usr/src/app/node_modules ./node_modules
COPY --chown=ben:root --from=build /usr/src/app/dist ./dist

CMD ["node", "dist/main.js"]


###### nếu không muốn sử dụng user non-root ######
# Build một container dựa trên image node:20-alpine
# Stage này dùng cho môi trường development
# FROM node:20-alpine As development

# # Tạo thư mục /usr/src/app trong container
# WORKDIR /usr/src/app

# # Copy package.json và package-lock.json vào container
# COPY package*.json .

# # Cài đặt dependencies của dự án từ package-lock.json 
# RUN npm ci

# # Copy toàn bộ code của dự án vào container
# COPY . .

# # Khai báo cổng 8888 mà ứng dụng Node.js sẽ lắng nghe bên trong container
# EXPOSE 8888

# # Chạy lệnh "node index.js" khi container được khởi động
# CMD ["node", "index.js"]

# # >>>>> build production <<<<< #
# FROM node:20-alpine As production

# >>>>>> COPY . . và COPY . ./ sẽ đều vào thư mục hiện tại,
# nếu muốn vào file root thì COPY . /
